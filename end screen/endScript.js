document.addEventListener("DOMContentLoaded", function() {
    window.onload = function() {
        $("#spinner").hide()
        $("#main").animate({"opacity": "1"},500)
        $("#close").click(function () {
            $("#main").addClass("animate__animated animate__zoomOutDown")
            setTimeout(() => {
                window.close()
            }, 1000);
        })
        $("#playAgain").click(function () {
            window.location.replace("../game/game.html")
        })    
    }
})

