var start = false

var rand = 100

var myInter

var currPage = 0

var scoreToFinish
var score

var index

var lomDone = false
var transitioning = false
document.addEventListener("DOMContentLoaded", function() {
    init()
});

function init() {
    window.onload = function() {
        $("#spinner").hide()
        $("#openScreen").fadeIn(500)
        if (sessionStorage.getItem("currPage") == null) {
            $("#toxic").delay(1000).animate({
                "height":"55vh",
                "width":"25vw",
                "margin-right": "-3vw"
            },1000)
            $("#openText").hide().delay(2000).show(1000)
            $("#text").delay(4000).animate({
                "margin-bottom": "4vw"
            },1000)
            $("#startButton").delay(4000).addClass("spinIn").on( "click", function() {
                startAnim()
            })
            setTimeout(() => {
                smoke()
            }, 3000);
        }
        else {
            backtolomda()
        }        
    }
}

function smoke() {
    myInter = setInterval(() => {
        setTimeout(() => {
            $("#smoke").append("<img src='media/cloud-boom.png' class='smoke'>")
            setTimeout(() => {
                document.getElementById("smoke").removeChild(document.getElementById("smoke").firstChild);
            }, 4000);
            rand = Math.floor(Math.random() * 800) + 500;             
        }, rand)
    }, 500)
}

function startAnim() {
    $("#startButton").off()
    clearInterval(myInter)
    $("#smoke").hide(1000)
    $("#openScreen").addClass("animate__zoomOutDown")
    setTimeout(() => {
        $("#logo").fadeIn()
    }, 1500);
    setTimeout(function(){
        $("#openScreen").fadeOut()
        $("#lomda").fadeIn(1000)
        $("#page0").fadeIn(1000)
        $("#progress").fadeIn(1000)
        $("#bubble").on("click", startLom)
    }, 2000)
}

function startLom() {
    $("#bubble").hide()
    $("#bubble").off()
    $("#progBubble").animate({
        top: "0.5vh",
        right: "0vw",
        height: "15vh"
    },500)
        $("#progBubble").fadeIn()
        setContent()
}

function backtolomda() {
    $("#openScreen").hide()
    $("#lomda").show()
    $("#progress").show()
    $("#bubble").hide()
    $("#progBubble").css({
        top: "1vh",
        right: "0vw",
        height: "15vh"
    })
    currPage = sessionStorage.getItem("currPage")
    // for dev - auto page
    currPage = 6
    // 
    setContent()
}

function setContent(){
    if (currPage == 7 && !lomDone) {
        lomDone = true
        $("#page"+currPage).fadeOut()
        $("#page8").delay(500).fadeIn()
        $("#progBubble").fadeOut()
        $(".next").fadeOut()
        $("#gameButton").delay(1000).fadeIn(500)
        $("#icon").css({
            "filter": "grayscale(1)",
            "cursor": "default"
        })
        setTimeout(() => {
            enableButton("gameIcon")
        }, 1000);
        for (i = 8; i >= 0; i--) {        
            $(".progItem").eq(i).css({"transform": "scale(0.8)"})
        }
        $(".progItem").addClass("clickable animate__animated animate__tada animate__delay-1s").click(function () {
            if (!transitioning && currPage != $(".progItem").index(this)) {
                transitioning = true
                setTimeout(() => {
                    transitioning = false
                }, 1200);
                index = $(".progItem").index(this)
                $("#progBubble").show().animate({"top": (index*12.33 + 0.5)+"vh"},500)
                $(".progItem").eq(index).css({"transform": "scale(1)"})
                for (i = 8; i >= 0; i--) {        
                    $(".progItem").eq(i).css({"transform": "scale(0.8)"})
                    if (i == index)
                    $(".progItem").eq(i).css({"transform": "scale(1)"})
                }
                sessionStorage.setItem("currPage", index);
                setContent()
            }
        })
    } 
    if (!lomDone) {
        $("#progBubble").animate({"top": (currPage*12.33 + 0.5)+"vh"},500)
    }
    for (i = currPage; i >= 0; i--) {        
        $(".progItem").eq(i).animate({"opacity": "100%"},1000)
        if (i != currPage && !lomDone) {
            $(".progItem").eq(i).css({"transform": "scale(0.8)"})
        }
    }
    $(".next").removeClass("nextEnabled animate__tada").off("click")
    
    setTimeout(() => {
        $("#mainScreen").fadeOut()
        setTimeout(() => {
            $("#logo").fadeIn()
            if (lomDone) {
                $("#page"+(currPage+1)).hide()
                currPage = Number(sessionStorage.getItem("currPage"))
                $("#page"+(currPage+1)).show()
            } else {
                $("#page"+currPage).hide()
                currPage++
                $("#page"+currPage).show()
            }
            $("#mainScreen").fadeIn(800)
            if (lomDone)
            (eval("checkFinish" + currPage))()
            else 
            (eval("checkFinish" + (currPage-1)))()
        }, 800);
    }, 100); 
    
}

function checkFinish0() {
    score = 0
    scoreToFinish = 3
    done = false
    $(".flip-card").hover(function () {
        if (!this.hovered) {
            score++
            if (score == scoreToFinish && !done) {
                enableButton("next")
                done = true
            }
        }
        this.hovered = true
    })
    
}

function checkFinish1() {
    score = 0
    scoreToFinish = 3
    clicked = false
    done = false
    $("#switchToggle").off().click(function () {
        $("#switchPoint").removeClass()
        if ($('#switchToggle').is(":checked"))
        {
            $("#warnings").toggleClass("scaleDown")
            $("#areas").toggleClass("scaleDown")
            $("#areas").css("height", "50vh")
            $('[data-toggle="tooltip"]').tooltip()
            $('[data-toggle="tooltip"]').tooltip("enable")
            
        } else {
            $("#warnings").toggleClass("scaleDown")
            $("#areas").toggleClass("scaleDown")
            $("#areas").css("height", "10vh")
            $('[data-toggle="tooltip"]').tooltip("disable")
        }
        $(".title").eq(2).css("color", "transparent")
        clicked = !clicked
        if (!clicked) {
            setTimeout(() => {
                $(".title").eq(2).text("נזקי החומרים המסוכנים").css("color", "rgb(133, 1, 1)")
            }, 300) 
        }
        else {
            setTimeout(() => {
                $(".title").eq(2).text('אזורי הסיכון באירוע חומ"ס').css("color", "rgb(133, 1, 1)")
            }, 300) 
        }
        
        $(".zone").hover(function () {
            if (!this.hovered && clicked) {
                score++
                if (score == scoreToFinish && !done) {
                    enableButton("next")
                    done = true
                }
                this.hovered = true
            }
        })
        $("#hotzone").hover(function () {
            $("#posherzone").tooltip("hide")
        }).mouseout(function () {
            $("#posherzone").tooltip("show")
        })
    })
}

function checkFinish2() {
    done = false
    pressed = {
        1: false,
        2: false,
        3: false
    }
    score = 0
    scoreToFinish = 3
    $( "#accordion" ).accordion({
        heightStyle: "content",
        active: 0
    });
    $(".downArrow").click(function () {
        index = $(".downArrow").index(this)
        $( "#accordion" ).accordion("option", "active", index+1)
        if (!pressed[index]) {
            score++
            pressed[index] = true
        }
    })
    $(".accorHeader").click(function () {
        index = $(".accorHeader").index(this)
        if (!pressed[index]) {
            score++
            pressed[index] = true
        }
    })
    $("#accordion").click(function(){
        if (score == scoreToFinish && !done) {
            enableButton("next")
            done = true
        }
    })
}

function checkFinish3() {
    done = false
    score = 0
    scoreToFinish = 4
    $(".collapse").collapse("hide")
    $(".btn").off().removeClass("colOpen").click(function () {
        if (!this.animating) {
            this.animating = true
            $(this).toggleClass("colOpen")
            setTimeout(() => {
                this.animating = false
            }, 500);
        }
        // $(".row").find('.collapse').collapse('hide');
        if (!this.clicked) {
            score++
        }
        this.clicked = true
        if (score == scoreToFinish && !done) {
            enableButton("next")
            done = true
        }
    })
}

function checkFinish4() {

    $('[data-toggle="tooltip"]').tooltip()
    
    score = 0
    scoreToFinish = 5
    done = false
    shownScreen = undefined

    screen = document.getElementById("stateScreen")
    
    $(".state").off().click(function () {        
        if (this.id != shownScreen) {
            if (this.id == "shigra") {
                $(screen).animate({"color": "rgba(0, 0, 0, 0)", "background-color": "rgba(76, 196, 76, 0.1)"},200)
                setTimeout(() => {
                    $(screen).text('פיקוד העורף מוסמך לדרוש ממחזיקי חומ"ס:  למסור מידע על החומ”ס שהם מחזיקים. להנחות אותם לנקוט אמצעים שבהתקיים מצב מיוחד בעורף ימנעו סכנה לחיי אדם או לבריאותו או לפגיעה ברכוש בשל החומ"ס. להנחות אותם בנוגע לדרכי האחסנה של החומרים המסוכנים.')
                    $(screen).css("color", "black")                        
                }, 400);
                if (!this.visited) {
                    score++
                }
                this.visited = true
            }
            if (this.id == "maavar") {
                $(screen).animate({"color": "rgba(0, 0, 0, 0)", "background-color": "rgba(255, 255, 0, 0.1)"},200)
                setTimeout(() => {
                    $(screen).text('פעילות פיקוד העורף מתמקדת במניעה והפחתה של אירועי החומ"ס והשלכותיהם בחירום.')
                    $(screen).css("color", "black")                        
                }, 400);
                if (!this.visited) {
                    score++
                }
                this.visited = true
            }
            if (this.id == "herum") {
                $(screen).animate({"color": "rgba(0, 0, 0, 0)", "background-color": "rgba(255, 77, 77, 0.1)"},200)
                setTimeout(() => {
                    $(screen).text('פיקוד העורף מוסמך לתת הנחיות משלימות למחזיקי חומ"ס בצורת צווים על מנת למזער את הסיכונים, לבצע ביקורות אכיפה על מימוש הצווים, להגביל שינוע של חומרים מסוכנים. לפיקוד העורף מוקנית סמכות לבצע את הצו במקום המחזיק לרבות פינוי החומרים המסוכנים, מכירתם ו/או השמדתם וסגירת המפעל.')
                    $(screen).css("color", "black")                        
                }, 400);
                if (!this.visited) {
                    score++
                }
                this.visited = true
            }
            if (score == scoreToFinish && !done) {
                enableButton("next")
                done = true
            }
            shownScreen = this.id
        }
    })
    $(".page5tooltip").hover(function () {
        if (!this.hovered) {
            score++
            if (score == scoreToFinish && !done) {
                enableButton("next")
                done = true
            }
        }
        this.hovered = true
    })
}

function checkFinish5() {
    done = false
    score = 0
    scoreToFinish = 6
    
    var backImg = new Image()
    backImg.src = "media/team2.svg"
    backImg.onload = ()=> {
        $(".col6").addClass("backImgShow")
    }
    
    $(".row6").css("width", "0%")
    $(".content6").css("color", "transparent")
    document.getElementsByClassName("btn6").open = false
    
    $(".btn6").off().click(function () {
        if (!this.clicked) {
            score++
            this.clicked = true
        }

        if (!this.open) {
            $(".row6").eq($(".btn6").index(this)).css("width", "100%")
            setTimeout(() => {
                $(".content6").eq($(".btn6").index(this)).css("color", "black")
            }, 400)
            this.open = true
        } else {
            $(".row6").eq($(".btn6").index(this)).css("width", "0%")
            $(".content6").eq($(".btn6").index(this)).css("color", "transparent")
            this.open = false
        }  
        for (i = 0; i <= 5; i++) {
            if ($(".btn6").index(this) != i) {
                $(".row6").eq(i).css("width", "0%")
                $(".content6").eq(i).css("color", "transparent")
                document.getElementsByClassName("btn6")[i].open = false
            }
        }
        if (score == scoreToFinish && !done) {
            enableButton("next")
            done = true
        }
    })

}

function checkFinish6() {
    done = false
    score = 0
    scoreToFinish = 5

    animating = false

    $(".shlav").css("filter", "brightness(100%)")
    $(".shlavContent").hide()
    $("#placeholder").delay(500).show()

    $(".shlav").off().click(function () {
        if (animating == false && index != $(".shlav").index(this)) {
            $(".shlav").eq(index).css("filter", "brightness(100%)")
            $(this).css("filter", "brightness(117%)")
            animating = true
            $("#wrapper").animate({
                top: "100%"
            },400)
            setTimeout(() => {
                $("#placeholder").hide()
                $(".shlavContent").eq(index).hide()
                index = $(".shlav").index(this)
                $(".shlavContent").eq(index).show()
                $("#wrapper").css({
                    top: "-100%"
                }).animate({
                    top: "0%"
                },500)
                setTimeout(() => {
                    animating = false
                }, 500);
            }, 500);
            if (!this.clicked) {
                score++
                this.clicked = true
            }
            if (score == scoreToFinish && !done) {
                enableButton("next")
                done = true
            }
        }
    })
    
}

function checkFinish7() {
}

function enableButton(buttonType) {
    if (buttonType == "gameIcon") {
        $("#icon").css({
            "filter": "grayscale(0)",
            "cursor": "pointer"
        }).addClass("animate__animated  animate__tada")      
        $("#icon").on("mouseover", function(){
            $("#gotogame").css("left", $("#gotogame").css("width"))
        }).on("mouseout", function(){
            $("#gotogame").css("left", "0vw")
        }).on("click", function(){
            sessionStorage.setItem("currPage", currPage);
            window.location.replace("game/game.html")
        })           
    } else if (buttonType == "next") {
        $(".next").addClass("nextEnabled animate__animated animate__tada").click(function(){
            sessionStorage.setItem("currPage", currPage);
            setContent()
        })          
    }
}


