var hu = screen.height/100
var wu = screen.width/100

var GRAVITY = -0.2*hu
var GRAVX = -0.01*wu

var showExplo1 = false 
var GameObjects = []

var currQ

// var answers1 = [true , false, true]
// var answers2 = [false, true, false]
// var answers3 = []
// var answers4 = []
// var answers5 = [false, false, false]
// var answers6 = []
// var answers7 = []

var answers = [true, false, true, false, true, false, false, true, true, false, true, true]
var questions = []


var checking = false
var barVal = 0

var startedGame = false

function setBinds() {
    $(document).keydown(function (event) {
        var key = (event.keyCode ? event.keyCode : event.which)


        // left
        if (key == '37') {
            mask.moveLeft = true
        }

        // right
        if (key == '39') {
            mask.moveRight = true
        }

        //up
        if (key == '38') {
            mask.jump()
        }

        //pickup
        if (key == '32') {
            pickup()
        }

    })
    $(document).keyup(function (event) {
        var key = (event.keyCode ? event.keyCode : event.which)

        // left
        if (key == '37') {
            mask.moveLeft = false
        }

        // right
        if (key == '39') {
            mask.moveRight = false
        }
        
    })

    
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|OperaMini/i.test(navigator.userAgent) ) {
        // document.documentElement.requestFullscreen()
        $("#mobileControls").fadeIn()

        touchRight = document.getElementById("touchRight")
        touchLeft = document.getElementById("touchLeft")
        pickupbutton = document.getElementById("pickup")

        touchRight.addEventListener('touchstart', function() {
            mask.moveRight = true
            $(this).css("opacity", "1")
        })
        touchRight.addEventListener('touchend', function() {
            mask.moveRight = false
            $(this).css("opacity", "0.7")
        })

        touchLeft.addEventListener('touchstart', function() {
            mask.moveLeft = true
            $(this).css("opacity", "1")
        })
        touchLeft.addEventListener('touchend', function() {
            mask.moveLeft = false
            $(this).css("opacity", "0.7")
        })

        pickupbutton.addEventListener('touchstart', function() {
            pickup()
            $(this).css("opacity", "1")
        })
        pickupbutton.addEventListener('touchend', function() {
            $(this).css("opacity", "0.7")
        })


    }


}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    while (0 !== currentIndex) {
  
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }  

function loadQuestionImages() {
    
    Q1_IMG = new Image()
    Q1_IMG.src = "media/Q1.png"
    Q2_IMG = new Image()
    Q2_IMG.src = "media/Q2.png"
    Q3_IMG = new Image()
    Q3_IMG.src = "media/Q3.png"
    Q4_IMG = new Image()
    Q4_IMG.src = "media/Q4.png"
    Q5_IMG = new Image()
    Q5_IMG.src = "media/Q5.png"
    Q6_IMG = new Image()
    Q6_IMG.src = "media/Q6.png"
    Q7_IMG = new Image()
    Q7_IMG.src = "media/Q7.png"
    Q8_IMG = new Image()
    Q8_IMG.src = "media/Q8.png"
    Q9_IMG = new Image()
    Q9_IMG.src = "media/Q9.png"
    Q10_IMG = new Image()
    Q10_IMG.src = "media/Q10.png"
    Q11_IMG = new Image()
    Q11_IMG.src = "media/Q11.png"
    Q12_IMG = new Image()
    Q12_IMG.src = "media/Q12.png"
}


document.addEventListener("DOMContentLoaded", function() {
    window.onload = function() {
        $("body").css("opacity", "1")
        
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|OperaMini/i.test(navigator.userAgent) ) {
            $("#mobile").show()
        } else {
            $("#pc").show()
        }
        init()
    }

})

    
function init(){
    for (i = 1; i <= answers.length; i++) {
        questions.push(i)
    }
    questions = shuffle(questions)
    currQ = 0


    canvas = document.getElementById("canvas")
    canvas.setAttribute("width", screen.width)
    canvas.setAttribute("height", screen.height)
    context = canvas.getContext("2d")

    MASK_IMG = new Image()
    MASK_IMG.src = "media/among.png"
    
    MASK_IMG2 = new Image()
    MASK_IMG2.src = "media/among2.png"

    mask = new GameObject(MASK_IMG, 47.5*wu, -100*hu, 5*wu, 12*hu)

    BOX_IMG = new Image()
    BOX_IMG.src = "media/square.png"

    box = new Box (BOX_IMG, 47.5*wu, 10*hu, 5*wu, 10*hu)
    box2 = new Box (BOX_IMG, 47.5*wu, 10*hu, 5*wu, 10*hu)


    SPACE_IMG = new Image()
    SPACE_IMG.src = "media/spaceF.png"

    SPACEno_IMG = new Image()
    SPACEno_IMG.src = "media/space-no.png"

    space = new GameObject(SPACE_IMG, 100*wu, -85*hu, 6*wu, 4*hu)

    BACK_IMG = new Image()
    BACK_IMG.src = "media/light2.png"

    back = new GameObject(BACK_IMG, 0*wu, 0*hu, 100*wu, 100*hu)

    PIPE_IMG = new Image()
    PIPE_IMG.src = "media/pipe.png"
    
    PIPE_IMG2 = new Image()
    PIPE_IMG2.src = "media/pipe2.png"

    pipe = new GameObject(PIPE_IMG, 0*wu, -5*hu, 15*wu, 30*hu)
    pipe2 = new GameObject(PIPE_IMG2, 85*wu, -5*hu, 15*wu, 30*hu)

    TOPPIPE_IMG = new Image()
    TOPPIPE_IMG.src = "media/topPipe.png"

    topPipe = new GameObject(TOPPIPE_IMG, 45*wu, 10*hu, 10*wu, 20*hu)


    loadQuestionImages()
    
    q = new GameObject(eval("Q"+questions[currQ]+"_IMG"), 45*wu, -80*hu, 22*wu, 13*hu)

    bar1 = new ldBar("#myProg")
    
    $("#mobileControls").hide()
    $("body").hide().fadeIn(500)

    $("#startButton").click(function () {
        if (!mask.startedJumping){
            mask.speedY = 3*hu
            mask.startedJumping = true
        }
        setTimeout(() => {
            startedGame = true
        }, 100);
        setTimeout(() => {
            box.thrown = true
        }, 1000);    
        $("#startButton").fadeOut()
        setBinds()
    })


    window.requestAnimationFrame(run)

}

function run(milis) {
    $("#myProg").css("transform", "scale("+0.0015*$(window).width()+")")
    update()
    render()
    window.requestAnimationFrame(run)
}

function GameObject(img, x, y, width, height) {
    this.img = img
    this.x = x
    this.y = y
    this.width = width
    this.height = height
    this.faceRight = true
    this.speedY = 0
    this.speedX = 0
    this.startedJumping = false
    this.thrown = false
    this.canPickup = false
    this.pickingUp = false
    this.showSpace = false
    setTimeout(() => {
        this.answer = answers[questions[currQ]-1]
    }, 1000)

    this.willCollideRight = function () {
        if (this.x + this.width >= canvas.width ) {
            //cant move right
            return true
        } else {
            return false
        }
    }

    this.willCollideLeft = function () {
        if (this.x <= 0) {
            //cant move left
            return true
        } else {
            return false
        }
    }

    this.willCollideBottom = function () {
        if (!startedGame) {
            return false
        }
        if (-(this.y + this.speedY - 12*hu) >= screen.height) {
            return true
        }
        return false
    }

    GameObjects.push(this)

}

function Box(img, x, y, width, height, name) {
    GameObject.call(this, img, x, y, width, height, name)
}

Box.prototype = Object.create(GameObject.prototype)
Box.prototype.constructor = Box


function update() {



    if (mask.moveLeft && !mask.willCollideLeft()){
        mask.x -= wu/1
        mask.img = MASK_IMG2
        mask.faceRight = false
    } 

    if (mask.moveRight && !mask.willCollideRight()) {
        mask.x += wu/1
        mask.img = MASK_IMG
        mask.faceRight = true
    }

    if (mask.startedJumping) {
        if (!mask.willCollideBottom()) {
            if (box.land() || box2.land()) {
                mask.y = -80*hu
                mask.startedJumping = false
                
            } else {
                mask.speedY += GRAVITY
                mask.y += mask.speedY
            }
        } 
        else {
            mask.y = -88*hu
            mask.speedY = 0
            mask.startedJumping = false
        }
    } 
    if (!(mask.x + mask.width >= box2.x && mask.x <= box2.x + box2.width) && !(mask.x + mask.width >= box.x && mask.x <= box.x + box.width) && mask.y == -80*hu) {
        mask.startedJumping = true

    }

    box.update()
    box2.update()

    if (box.showSpace || box2.showSpace) {
        space.img = SPACE_IMG
    } else {
        space.img = SPACEno_IMG

    }
}

Box.prototype.update = function () {


    if (this.thrown) {
        if (!this.willCollideBottom()) {
            this.speedY += GRAVITY/4
            this.y += this.speedY
        } else {
            this.y = -90*hu
            this.speedY = 0
        }
        if (this.speedX < 0 && this.speedX - GRAVX < 0) {
            this.x += this.speedX
            this.speedX -= GRAVX
        }
        else if (this.speedX > 0 && this.speedX - GRAVX > 0) {
            this.x += this.speedX
            this.speedX += GRAVX
        } 
        else {
            this.speedX = 0
        }
    }

    
    if (this.x <= 0 && !this.pickingUp && checking == false) {
        this.width -= 0.1*wu
        q.width = 0
        setTimeout(() => {
            this.speedX = 0
            if (q.answer) {
                this.width = 5*wu
                this.y = -14*hu
                this.speedY = 0
                this.x = 9*wu
                setTimeout(() => {
                    q.width = 22*wu
                    checking = false
                }, 500)
                $("#noX").show()
                setTimeout(() => {
                    $("#noX").hide()
                }, 500);
            } else {
                barVal += 100/answers.length
                bar1.set(barVal)
                $("#myProg").attr('data-content',`${currQ+1}/12`);
                $("#noV").show()
                setTimeout(() => {
                    $("#noV").hide()
                    if (answers.length != currQ+1) {
                        currQ++ 
                        
                        q.img = eval("Q"+questions[currQ]+"_IMG")            
                        q.answer = answers[questions[currQ]-1]
                        this.width = 5*wu
                        this.y = 10*hu
                        this.speedY = 0
                        this.x = 47.5*wu
                        setTimeout(() => {
                            q.width = 22*wu
                            checking = false
                        }, 400)
                    } else {
                        //win
                        $("#victory").fadeIn()
                        setTimeout(() => {
                            $("body").fadeOut()
                            setTimeout(() => {
                                window.location.replace("../end screen/endScreen.html")
                            }, 500);
                        }, 2000);
                    }
                }, 500)
            }
        }, 500)
        checking = true
    }

    if (this.x >= screen.width - this.width && !this.pickingUp && checking == false) {
        this.width -= 0.1*wu
        q.width = 0
        this.x += 0.1*wu
        setTimeout(() => {
            this.speedX = 0
            if (!q.answer) {
                this.width = 5*wu
                this.y = -14*hu
                this.speedY = 0
                this.x = screen.width-9*wu-this.width
                setTimeout(() => {
                    q.width = 22*wu
                    checking = false
                }, 500)
                $("#yesX").show()
                setTimeout(() => {
                    $("#yesX").hide()
                }, 500);
            } else {
                barVal += 100/answers.length
                bar1.set(barVal)
                $("#myProg").attr('data-content',`${currQ+1}/12`);
                $("#yesV").show()
                setTimeout(() => {
                    $("#yesV").hide()
                    if (answers.length != currQ+1) {
                        currQ++ 
                        
                        q.img = eval("Q"+questions[currQ]+"_IMG")
                        q.answer = answers[questions[currQ]-1]
                        this.width = 5*wu
                        this.y = 10*hu
                        this.speedY = 0
                        this.x = 47.5*wu
                        setTimeout(() => {
                            q.width = 22*wu
                            checking = false
                        }, 400)
                    } else {
                        //win
                        $("#victory").fadeIn()
                        setTimeout(() => {
                            $("body").fadeOut()
                            setTimeout(() => {
                                window.location.replace("../end screen/endScreen.html")
                            }, 500);
                        }, 2000);
                    }
                }, 500)
            }
        }, 500)
        checking = true
    }
    
    if (mask.x + mask.width >= this.x && mask.x <= this.x + this.width && mask.y == -88*hu && this.y == -90*hu && this.width == 5*wu) {
        this.canPickup = true
    }
    else {
        this.canPickup = false
    }

    if (this.pickingUp) {
        if (mask.faceRight) 
            this.x = mask.x + mask.width
        if (!mask.faceRight)
            this.x = mask.x - mask.width
        this.y = mask.y
    }

    if (mask.x + mask.width >= this.x && mask.x <= this.x + this.width && (mask.y-2*hu == this.y || mask.y == this.y) && this.width == 5*wu) {
        this.showSpace = true
    } else {
        this.showSpace = false
    }
   
}

Box.prototype.land = function () {
    if (mask.x + mask.width >= this.x && mask.x <= this.x + this.width && mask.speedY < 0 && mask.y - mask.height < this.y + 0.5*hu && !this.pickingUp && this.y == -90*hu) {
        return true
    }
    return false
}

function render() {
    context.clearRect(0, 0, canvas.width, canvas.height)
    
    context.drawImage(box.img, box.x, -box.y, box.width, box.height)
    
    context.drawImage(q.img, box.x-((q.width/2)-(box.width/2)), -box.y-17*hu, q.width, q.height)
    
    context.drawImage(mask.img, mask.x, -mask.y, mask.width, mask.height)
    
    context.drawImage(space.img, mask.x, -mask.y-4*hu, space.width, space.height)
    
    context.drawImage(pipe.img, pipe.x, -pipe.y-5*hu, pipe.width, pipe.height)
    context.drawImage(pipe2.img, pipe2.x, -pipe2.y-5*hu, pipe2.width, pipe2.height)
    
    context.drawImage(topPipe.img, topPipe.x, -topPipe.y, topPipe.width, topPipe.height)
    
    context.drawImage(back.img, back.x, -back.y, back.width, back.height)

}


GameObject.prototype.jump = function () {
    if (!this.startedJumping){
        this.speedY = 2.5*hu
        this.startedJumping = true
    }
}

function pickup () {
    if (box.pickingUp)
    {
        box.throw()
    } else if (box.canPickup && !box2.canPickup) {
        box.pickingUp = true
    }

    if (box2.pickingUp)
    {
        box2.throw()
    } else if (box2.canPickup) {
        box2.pickingUp = true
    }

}

GameObject.prototype.throw = function () {
    this.pickingUp = false
    this.thrown = true
    this.speedY = 1*hu
    if (mask.faceRight) {
        this.speedX = 1*wu
    } else if (!mask.faceRight) {
        this.speedX = -1*wu
    }
}
